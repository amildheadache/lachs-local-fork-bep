﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public float fireRate;
    public float direction;

    private float nextFire;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 10f;
        transform.Translate(x, 0, 0);
        Vector3 clampedPos = transform.position;
        clampedPos.x = Mathf.Clamp(transform.position.x, -7f, 7f);
        transform.position = clampedPos;

        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
            Debug.Log(bulletSpawn.position);
            //GameObject clone = Instantiate (projectile, transform.position, transform.rotation) as GameObject
        }
    }

    //https://unity3d.com/learn/tutorials/temas/multiplayer-networking/shooting-single-player
}
