﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareMoveScript : MonoBehaviour {
    public Vector3 mousePos;
    public float speed = 2.0F;
    // Use this for initialization
    void Start() {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 diff = mousePos - transform.position;
        diff.z = 0;
        if (diff.x >= 0.1 || diff.y >= 0.1 || diff.x <= -0.1 || diff.y <= -0.1)
        {
            diff.Normalize();
            transform.Translate(diff * Time.deltaTime*speed);
        }
		
	}
}
