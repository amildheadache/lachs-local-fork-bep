﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed = 15.0f;
    public float padding = 0.51f;
    public float projectileSpeed;
    public float firingRate = 0.2f;
    public float health = 250f;

    public GameObject projectile;

    float xmin;
    float xmax;

    void Start()
    {
        //calculating the viewport's size for auto calculations of Mathf.Clamp
        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xmin = leftmost.x + padding;
        xmax = rightmost.x - padding;
    }

    void Fire()
    {
        Vector3 offset = new Vector3(0, 1, 0);
        GameObject beam = Instantiate(projectile, transform.position + offset, Quaternion.identity) as GameObject;
        beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
    }

    // Update is called once per frame
	void Update () {

        //Continuous yet delayed bullet fire so it will not show solid lines fire, but bursts of firing
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.000001f, firingRate);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //ensure movement is independent of framerate using Time.deltaTime
            transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);

        } else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
        }

        //restricts player to the gamespace
        float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        //grab gameObject collider, get projectile component 
        ProjectileScript missile = collider.gameObject.GetComponent<ProjectileScript>();
        if (missile)
        {
            Debug.Log("Player died");
            health -= missile.GetDamage();
            //tell missile its collided
            missile.Hit();
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
