﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public GameObject enemyPrefab;
    public float width = 10f;
    public float height = 6f;
    public float speed = 5f;

    //delay half a second
    public float spawnDelay = 0.5f;

    private bool movingRight = true;
    private float xmax;
    private float xmin;

	// Use this for initialization
	void Start () {
        float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftBoundary = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceToCamera));
        Vector3 rightBoundary = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distanceToCamera));
        xmax = rightBoundary.x;
        xmin = leftBoundary.x;

        spawnEnemies();
	}

    public void spawnEnemies ()
    {
        //spawns enemies in each position GameObject
        foreach (Transform child in transform)
        {
            // Quaternion.identity: no rotation
            //instantiate creates object not GameObject, hence 'as GameObject' at end
            GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;

            //Enemies to spawn within Position GameObject
            enemy.transform.parent = child;
        }
    }

    //spawn enemies one at a time at empty position
    void SpawnUntilFull()
    {
        Transform freePosition = NextFreePosition();
        if (freePosition) {
            GameObject enemy = Instantiate(enemyPrefab, freePosition.position, Quaternion.identity) as GameObject;
            enemy.transform.parent = freePosition;
        }

        if (NextFreePosition()) {
            Invoke("SpawnUntilFull", spawnDelay);
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height, 0));
    }

    // Update is called once per frame
    void Update () {
        //enemies move side to side
		if (movingRight)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }else
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        //enemies move other side when reach edge of screen
        float rightEdgeOfFormation = transform.position.x + (0.5f * width);
        float leftEdgeOfFormation = transform.position.x - (0.5f * width);

        if (leftEdgeOfFormation < xmin)
        {
            movingRight = true;
        } else if (rightEdgeOfFormation > xmax)
        {
            movingRight = false;
        }

        if (AllMembersDead())
        {
            
            spawnEnemies();
        }
	}

    //Find empty position in enemy formation
    Transform NextFreePosition()
    {
        foreach (Transform childPositionGameObject in transform)
        {
            if (childPositionGameObject.childCount == 0)
            {
                return childPositionGameObject;
            }
        }
        return null;
    }

    //Checks if enemy formation is empty
    bool AllMembersDead()
    {
        //going over every child position in the transform of enemy formation
        foreach(Transform childPositionGameObject in transform) {
            if (childPositionGameObject.childCount > 0)
                return false;
        }
        return true;
    }
}
